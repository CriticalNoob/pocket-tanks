package enviroment;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import weapons.Cutter;
import weapons.SingleShot;
import weapons.WeaponType;

public class Controls extends HBox{

	private Path arrowUp, arrowDown, arrowRight, arrowLeft;
	private final double ARROW_SIZE = 4;
	private Button upBtn, downBtn, leftBtn, rightBtn;
	private Button btn = new Button("OK");
	private Label lMoves;
	public Label angle;
	private int angleNmb = 90;
	private Slider slider;
	private Label sliderValue;
	private ChoiceBox cb;
	private Integer moves = 5;
	private WeaponType type;
	public Cutter cut = new Cutter();
	public SingleShot single = new SingleShot();
	public Button endBtn = new Button();
	
	public Button getEndBtn() {
		return endBtn;
	}

	public Button getBtn() {
		return btn;
	}

	public Controls() {
		arrowUp = new Path();
		arrowUp.getElements().addAll(new MoveTo(-ARROW_SIZE, 0), new LineTo(ARROW_SIZE, 0), 
								new LineTo(0 , -ARROW_SIZE), new LineTo(-ARROW_SIZE, 0));
		arrowUp.setMouseTransparent(true);
		arrowDown = new Path();
		arrowDown.getElements().addAll(new MoveTo(-ARROW_SIZE, 0), new LineTo(ARROW_SIZE, 0), 
								new LineTo(0 , ARROW_SIZE), new LineTo(-ARROW_SIZE, 0));
		arrowDown.setMouseTransparent(true);
		arrowRight = new Path();
		arrowRight.getElements().addAll(new MoveTo(0, -ARROW_SIZE), new LineTo(0, ARROW_SIZE), 
								new LineTo(ARROW_SIZE, 0), new LineTo(0, -ARROW_SIZE));
		arrowRight.setMouseTransparent(true);
		arrowLeft = new Path();
		arrowLeft.getElements().addAll(new MoveTo(0, -ARROW_SIZE), new LineTo(0, ARROW_SIZE), 
								new LineTo(-ARROW_SIZE , 0), new LineTo(0, -ARROW_SIZE));
		arrowLeft.setMouseTransparent(true);
		leftBtn = new Button();
		//sledeca 2 reda test za event
		leftBtn.setId("left");
		rightBtn = new Button();
		
		upBtn = new Button();
		downBtn = new Button();
		angle = new Label();
		angle.setText("Angle: \n" + angleNmb);
		angle.setAlignment(Pos.CENTER);
		
		slider = new Slider();
		slider.setMin(1);
		slider.setMax(100);
		slider.setValue(50);
		slider.setBlockIncrement(1);
		sliderValue = new Label("Power: " + (int)slider.getValue());
		
		StackPane leftPane = new StackPane();
		leftPane.getChildren().addAll(leftBtn, arrowLeft);
		leftPane.setAlignment(Pos.CENTER);
		StackPane rightPane = new StackPane();
		rightPane.getChildren().addAll(rightBtn, arrowRight);
		rightPane.setAlignment(Pos.CENTER);
		
		StackPane incPane = new StackPane();
		incPane.getChildren().addAll(upBtn, arrowUp);
		incPane.setAlignment(Pos.CENTER);
		StackPane decPane = new StackPane();
		decPane.getChildren().addAll(downBtn, arrowDown);
		decPane.setAlignment(Pos.CENTER);
		
		VBox buttons = new VBox();
		buttons.getChildren().addAll(incPane, decPane);
		buttons.setAlignment(Pos.CENTER);
		HBox buttons2 = new HBox();
		buttons2.getChildren().addAll(leftPane, rightPane);
		
		cb = new ChoiceBox();
		for(int i = 0; i < 3; i++) {
			cb.getItems().add(type.getName(WeaponType.SINGLE_SHOT));
			cb.getItems().add(type.getName(WeaponType.CUTTER));
		}
		cb.getSelectionModel().selectFirst();

		
		lMoves = new Label("Moves: \n" + moves);
		
		this.setSpacing(20);
		this.getChildren().addAll(cb, angle, buttons, lMoves, buttons2, slider, sliderValue, btn);
		this.setAlignment(Pos.CENTER);
		this.setPrefSize(1200, 200);
		this.setLayoutX(0);
		this.setLayoutY(600);
	
	}
	
	

	public ChoiceBox getCb() {
		return cb;
	}

	public int getAngleNmb() {
		return angleNmb;
	}

	public void setAngleNmb(int angleNmb) {
		this.angleNmb += angleNmb;
	}

	public void setlMoves() {
		--moves;
		this.lMoves.setText("Moves: " + moves);
		
	}
	public Label getSliderValue() {
		return sliderValue;
	}
	public Slider getSlider() {
		return slider;
	}
	public Button getUpBtn() {
		return upBtn;
	}
	public Button getDownBtn() {
		return downBtn;
	}
	public Button getRightBtn() {
		return rightBtn;
	}
	public Button getLeftBtn() {
		return leftBtn;
	}
	public int getMoves() {
		return moves;
	}
}
