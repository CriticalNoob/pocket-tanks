package server;

import java.io.IOException;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class ServerApplication extends Application{

	public static void main(String[] args) {
		launch();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		try {
			Server server = new Server(9000);
			Thread serverThread = new Thread(server);
			serverThread.setName("Server Thread");
			serverThread.setDaemon(true);
			serverThread.start();
			
			primaryStage.setTitle("JavaFX Server");
			primaryStage.setScene(makeServerUI(server));
			primaryStage.show();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Scene makeServerUI(Server server){
		
		GridPane rootPane = new GridPane();
		rootPane.setAlignment(Pos.CENTER);
		rootPane.setPadding(new Insets(20));
		rootPane.setHgap(10);
		rootPane.setVgap(10);
		
		Label logLabel = new Label("Server Log");
		ListView<String> logView = new ListView<String>();
		ObservableList<String> logList = server.serverLog;
		logView.setItems(logList);
		
		Label client1Label = new Label("Client 1 Send");
		ListView<String> client1View = new ListView<String>();
		ObservableList<String> client1List = server.client1Send;
		client1View.setItems(client1List);
		
		Label client2Label = new Label("Client 2 Send");
		ListView<String> client2View = new ListView<String>();
		ObservableList<String> client2List = server.client2Send;
		client2View.setItems(client2List);
		
		rootPane.add(logLabel, 0, 0);
		rootPane.add(logView, 0, 1, 2, 1);
		rootPane.add(client1Label, 0, 2);
		rootPane.add(client1View, 0, 3, 2, 1);
		rootPane.add(client2Label, 0, 4);
		rootPane.add(client2View, 0, 5, 2, 1);
		
		return new Scene(rootPane, 400, 600);
	}
}
