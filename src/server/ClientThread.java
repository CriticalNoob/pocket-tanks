package server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import javafx.application.Platform;

public class ClientThread extends Thread{

	
	private Socket sock;
	private Server baseServer;
	private BufferedReader input;
	private PrintWriter output;
	private int value;
	private ClientThread opponent;

	public ClientThread(Socket sock, int value, Server baseServer) {
		this.sock = sock; 
		this.value = value;
		this.baseServer = baseServer;
		try { 	
			input = new BufferedReader(new InputStreamReader( sock.getInputStream())); 
			output = new PrintWriter(new BufferedWriter( new OutputStreamWriter( sock.getOutputStream())), true);
			if (value == 1) {
                output.println(value);
                Platform.runLater(new Runnable() {
    				@Override
    				public void run() {
    					baseServer.serverLog.add("Player 1: connected");
    				}
    			});
            }
			if(value == 2) {
				output.println(value);
				Platform.runLater(new Runnable() {
    				@Override
    				public void run() {
    					baseServer.serverLog.add("Player : connected");
    				}
    			});
			}
		} catch (IOException ex) { 
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					baseServer.serverLog.add("Player" + value + " : disconnected");
				}
			});
			try {
				input.close();
				output.close();
	        	sock.close();
			} catch (IOException e) {
				
				e.printStackTrace();
			}
        	
		} 
	}


	public void run() {
		try { 
			
			String command;
			
			while (true) {
                command = input.readLine();
                if(command.startsWith("UNLOCK")) { 
                	opponent.output.println("UNLOCK");
                }else if(command.startsWith("UP1")) { 
                	Platform.runLater(new Runnable() {
        				@Override
        				public void run() {
        					baseServer.client1Send.add("Player 1: angle +1");
        				}
        			});
                	opponent.output.println("UP1");
                } else if(command.startsWith("UP2")) {
                	Platform.runLater(new Runnable() {
        				@Override
        				public void run() {
        					baseServer.client2Send.add("Player 2: angle +1");
        				}
        			});
                	opponent.output.println("UP2");
                }else if(command.startsWith("DOWN1")) {
                	Platform.runLater(new Runnable() {
        				@Override
        				public void run() {
        					baseServer.client1Send.add("Player 1: angle -1");
        				}
        			});
                	opponent.output.println("DOWN1");
                }else if(command.startsWith("DOWN2")) {
                	Platform.runLater(new Runnable() {
        				@Override
        				public void run() {
        					baseServer.client2Send.add("Player 2: angle -1");
        				}
        			});
                	opponent.output.println("DOWN2");
                }else if(command.startsWith("LEFT1")) {
                	Platform.runLater(new Runnable() {
        				@Override
        				public void run() {
        					baseServer.client1Send.add("Player 1: moved left");
        				}
        			});
                	opponent.output.println("LEFT1");
                }else if(command.startsWith("LEFT2")) {
                	Platform.runLater(new Runnable() {
        				@Override
        				public void run() {
        					baseServer.client2Send.add("Player 2: moved left");
        				}
        			});
                	opponent.output.println("LEFT2");
                }else if(command.startsWith("RIGHT1")) {
                	Platform.runLater(new Runnable() {
        				@Override
        				public void run() {
        					baseServer.client1Send.add("Player 1: moved right");
        				}
        			});
                	opponent.output.println("RIGHT1");
                }else if(command.startsWith("RIGHT2")) {
                	Platform.runLater(new Runnable() {
        				@Override
        				public void run() {
        					baseServer.client2Send.add("Player 2: moved right");
        				}
        			});
                	opponent.output.println("RIGHT2");
                }else if(command.startsWith("OK1")) {
                	String[] splitter = command.split(",");
                	int angle = Integer.parseInt(splitter[2]);
                	int power = Integer.parseInt(splitter[1]);
                	int bulet = Integer.parseInt(splitter[3]);
                	Platform.runLater(new Runnable() {
        				@Override
        				public void run() {
        					if(bulet == 1) {
        						baseServer.client1Send.add("Player 1: fired Single Shot with angle: " + angle + ", power: " + power);
        					}else if(bulet == 2){
        						baseServer.client1Send.add("Player 1: fired Cutter with angle: " + angle + ", power: " + power);
        					}
        				}
        			});
                	opponent.output.println(command);
                }else if(command.startsWith("OK2")) {
                	String[] splitter = command.split(",");
                	int angle = Integer.parseInt(splitter[2]);
                	int power = Integer.parseInt(splitter[1]);
                	int bulet = Integer.parseInt(splitter[3]);
                	Platform.runLater(new Runnable() {
        				@Override
        				public void run() {
        					if(bulet == 1) {
        						baseServer.client2Send.add("Player 2: fired Single Shot with angle: " + angle + ", power: " + power);
        					}else if(bulet == 2){
        						baseServer.client2Send.add("Player 2: fired Cutter with angle: " + angle + ", power: " + power);
        					}
        				}
        			});
                	opponent.output.println(command);
                }else if(command.startsWith("OK3END")) {
                	String[] splitter = command.split(",");
                	int player1Score = Integer.parseInt(splitter[1]);
                	int player2Score = Integer.parseInt(splitter[2]);
                	if(player1Score > player2Score) {
                		Platform.runLater(new Runnable() {
            				@Override
            				public void run() {
            					baseServer.client1Send.add("Player 1 Won!");
            					baseServer.client2Send.add("Player 2 Lost!");
            					
            				}
            			});
                		output.println("END1,You Won!");
                    	opponent.output.println("END2,You Lost!");
                	}else if(player1Score < player2Score) {
                		Platform.runLater(new Runnable() {
            				@Override
            				public void run() {
            					baseServer.client1Send.add("Player 1 Lost!");
            					baseServer.client2Send.add("Player 2 Won!");
            					
            				}
            			});
                		output.println("END1,You Lost!");
                    	opponent.output.println("END2,You Won!");
                	}else if(player1Score == player2Score) {
                		Platform.runLater(new Runnable() {
            				@Override
            				public void run() {
            					baseServer.client1Send.add("DRAW!");
            					baseServer.client2Send.add("DRAW!");
            					
            				}
            			});
                		output.println("END1,DRAW!");
                		opponent.output.println("END2,DRAW!");
                	}
                	
                }else if(command.startsWith("OK2END")) {
                	String[] splitter = command.split(",");
                	int angle = Integer.parseInt(splitter[2]);
                	int power = Integer.parseInt(splitter[1]);
                	int bulet = Integer.parseInt(splitter[3]);
                	Platform.runLater(new Runnable() {
        				@Override
        				public void run() {
        					if(bulet == 1) {
        						baseServer.client2Send.add("Player 2: fired Single Shot with angle: " + angle + ", power: " + power);
        					}else if(bulet == 2){
        						baseServer.client2Send.add("Player 2: fired Cutter with angle: " + angle + ", power: " + power);
        					}
        				}
        			});
                	opponent.output.println(command);
                }
            }
			
			}catch (IOException e) {
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						baseServer.serverLog.add("Player" + value + " : disconnected");
					}
				});
                try {
					input.close();
					output.close();
	            	sock.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
            } 
	}

	
	public void setOpponent(ClientThread opponent) {
		this.opponent = opponent;
	}
	
}
