package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Server implements Runnable{


	private ServerSocket sock;
	private ArrayList<ClientThread> clients;
	public ObservableList<String> serverLog;
	public ObservableList<String> client1Send;
	public ObservableList<String> client2Send;
	public int clientCounter = 1;
	
	public Server(int portNumber) throws IOException {
		sock = new ServerSocket(portNumber);
		serverLog = FXCollections.observableArrayList();
		client1Send = FXCollections.observableArrayList();
		client2Send = FXCollections.observableArrayList();
	}
	
	public void run() {
		int clientNmb = 0;
		clients = new ArrayList<ClientThread>();
		try {
			int clientCounter = 0;
			
			while (true) {
				
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						serverLog.add("Listening for client");
					}
				});

				final Socket clientSocket = sock.accept();
				++clientCounter;
				if(clientCounter == 1) {
					ClientThread first = new ClientThread(clientSocket, ++clientNmb,this);
					clients.add(first);
					first.start();
				}else if(clientCounter == 2) {
					ClientThread second = new ClientThread(clientSocket, ++clientNmb,this);
					clients.add(second);
					second.start();
				}
				if(clientCounter == 2) {
					ClientThread a = clients.get(0);
					ClientThread b = clients.get(1);
					a.setOpponent(b);
					b.setOpponent(a);
				}
				
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						serverLog.add("Player connected from: " + clientSocket.getRemoteSocketAddress());
					}
				});
				
			}
		} catch (IOException e) {
			
			e.printStackTrace();
		}

	}
}
