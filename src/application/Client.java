package application;

import java.io.BufferedReader;
import enviroment.Controls;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

public class Client implements Runnable{

	private Socket clientSocket;
	private BufferedReader serverToClientReader;
	private PrintWriter clientToServerWriter;
	public String stored = "";
	public String stored2 = "";
	public Controls control;
	public String player1 = "";
	public String player2 = "";
	public double power = 0;
	public int angle = 0;
	public int bulet = 0;
	public String starts = "";
	public String end = "";
	
	public Controls getControl() {
		return control;
	}

	public Client(String hostName, int portNumber) throws UnknownHostException, IOException {
		
		clientSocket = new Socket(hostName, portNumber);
		serverToClientReader = new BufferedReader(new InputStreamReader(
				clientSocket.getInputStream()));
		clientToServerWriter = new PrintWriter(
				clientSocket.getOutputStream(), true);
		control = new Controls();
		
	}
	
	public void writeToServer(String input) {
		clientToServerWriter.println(input);
	}
	
	public void run() {
		try {
			stored = serverToClientReader.readLine();
			
			if(stored.startsWith("1")) {
				player1 = "1";
				end = "Ceka se drugi igrac da udje!";
				control.setDisable(true);
			}
			if(stored.startsWith("2")) {
				player2 = "2";
				end = "Cekanje na potez prvog igraca!";
				control.setDisable(true);
				this.writeToServer("UNLOCK");
				
			}
            
		while (true) {
			try {
				
				String inputFromServer = serverToClientReader.readLine();
				
						stored2 = inputFromServer;
						if(stored2.equals("UNLOCK")) {
							control.setDisable(false);
							control.getEndBtn().fire();
						}else if(stored2.equals("UP1")) {
							control.setDisable(false);
							control.getUpBtn().fire();
						}else if(stored2.equals("UP2")) {
							control.setDisable(false);
							control.getUpBtn().fire();
						}else if(stored2.equals("DOWN1")) {
							control.setDisable(false);
							control.getDownBtn().fire();
						}else if(stored2.equals("DOWN2")) {
							control.setDisable(false);
							control.getDownBtn().fire();
						}else if(stored2.equals("LEFT1")) {
							control.setDisable(false);
							control.getLeftBtn().fire();
						}else if(stored2.equals("LEFT2")) {
							control.setDisable(false);
							control.getLeftBtn().fire();
						}else if(stored2.equals("RIGHT1")) {
							control.setDisable(false);
							control.getRightBtn().fire();
						}else if(stored2.equals("RIGHT2")) {
							control.setDisable(false);
							control.getRightBtn().fire();
						}else if(stored2.startsWith("OK1")) {
							String[] splitter = stored2.split(",");
							starts = splitter[0];
							power = Double.parseDouble(splitter[1]);
							angle = Integer.parseInt(splitter[2]);
							bulet = Integer.parseInt(splitter[3]);
							control.setDisable(false);
							control.getBtn().fire();
						}else if(stored2.startsWith("OK2")) {
							String[] splitter = stored2.split(",");
							starts = splitter[0];
							power = Double.parseDouble(splitter[1]);
							angle = Integer.parseInt(splitter[2]);
							bulet = Integer.parseInt(splitter[3]);
							control.setDisable(false);
							control.getBtn().fire();
						}else if(stored2.startsWith("END1")) {
							String[] splitter = stored2.split(",");
							end = splitter[1];
							control.getEndBtn().fire();
							control.setDisable(true);
						}else if(stored2.startsWith("END2")) {
							String[] splitter = stored2.split(",");
							end = splitter[1];
							control.getEndBtn().fire();
							control.setDisable(true);
						}else if(stored2.startsWith("OK2END")) {
							String[] splitter = stored2.split(",");
							starts = splitter[0];
							power = Double.parseDouble(splitter[1]);
							angle = Integer.parseInt(splitter[2]);
							bulet = Integer.parseInt(splitter[3]);
							control.getBtn().fire();
						}
				}

			 catch (SocketException e) {
				 	e.printStackTrace();
				} catch (IOException e) {
				
				e.printStackTrace();
			}
		}
		}catch(Exception e) {
			e.printStackTrace();;
		}
	}
	
}
