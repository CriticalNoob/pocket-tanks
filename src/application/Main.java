package application;
	
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.stage.Stage;
import javafx.util.Duration;
import objects.Bullet;
import objects.PlayerTanks;
import objects.Tank;
import application.MenuItem;
import enviroment.GameMap;
import javafx.scene.*;
import javafx.scene.layout.*;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Rotate;

public class Main extends Application{
	
	private Stage stage;
	private Scene scene1, scene2;
	private MenuItem menu1, menu2;
	private VBox menuBox;
	private PlayerTanks player;
	private GameMap map = new GameMap();
	private Tank player1Tank = new Tank(player.Player1), player2Tank = new Tank(player.Player2);
	private TranslateTransition trTank = new TranslateTransition();
	private TranslateTransition trGun = new TranslateTransition();
	private Rotate rotation;
	private Bullet bullet;
	private static int TCP_PORT = 9000;
	private String test1 = "";
	private int bulet = 0;
	
	private Parent createContentScene1() {
		Pane root = new Pane();
		root.setPrefSize(1200, 800);
		
		Rectangle bg = new Rectangle(1200, 800);
		
		menu1 = new MenuItem("PLAY");
		menu1.setOnMouseClicked(e -> {
		Client client;
		Thread clientThread;
		try {
				client = new Client("localhost", TCP_PORT);
				clientThread = new Thread(client);
				clientThread.setDaemon(true);
				clientThread.start();
				stage.setScene(scene2 = new Scene(createContentScene2(client)));
		
		} catch (Exception e1) {
			
			e1.printStackTrace();
		}});
		menu1.setOnMouseEntered(e -> {menu1.changeColorRed();});
		menu1.setOnMouseExited(e -> {menu1.changeColorGrey();});
		
		menu2 = new MenuItem("EXIT");
		menu2.setOnMouseClicked(e -> {System.exit(0);});
		menu2.setOnMouseEntered(e -> {menu2.changeColorRed();});
		menu2.setOnMouseExited(e -> {menu2.changeColorGrey();});
		
		menuBox = new VBox(menu1,menu2);
		menuBox.setAlignment(Pos.TOP_CENTER);
		menuBox.setTranslateX(550);
		menuBox.setTranslateY(350);
		
		root.getChildren().addAll(bg, menuBox);
		
		return root;
	}
	
	private Parent createContentScene2(Client client) throws Exception{
		GameMenu game = new GameMenu(player1Tank, player2Tank, map, client.getControl());
		Platform.runLater(() ->{
			map.connection.setText(client.end);
			
		});
		
		client.getControl().getEndBtn().setOnAction(new EventHandler<javafx.event.ActionEvent>() {

			@Override
			public void handle(javafx.event.ActionEvent event) {
				test1 = client.stored2;
				if(test1.equals("UNLOCK")) {
					map.connection.setText("Vi ste na potezu!");
				}else if(client.stored2.startsWith("END2")) {
					map.connection.setText(client.end);
				}else if(client.stored2.startsWith("END1")) {
					map.connection.setText(client.end);
				}
				test1 = "";
				client.stored2 = "";
			}});
		
		//OK button
		client.getControl().getBtn().setOnAction(new EventHandler<javafx.event.ActionEvent>() {
			@Override
			public void handle(javafx.event.ActionEvent event) {
				test1 = client.starts;
				if(client.player1.equals("1") && test1.equals("")) {
					if(client.control.getCb().getSelectionModel().getSelectedItem().toString().equals("Single Shot")) {
						bullet = new Bullet(player1Tank.getImg().getTranslateX(), player1Tank.getImg().getTranslateY(),player1Tank.getImg().getFitWidth(), 1);
						bulet = 1;
					}
					if(client.control.getCb().getSelectionModel().getSelectedItem().toString().equals("Cutter")) {
						bullet = new Bullet(player1Tank.getImg().getTranslateX(), player1Tank.getImg().getTranslateY(),player1Tank.getImg().getFitWidth(), 2);
						bulet = 2;
					}
					double power = client.getControl().getSlider().getValue();
					double vx = Math.cos(Math.toRadians(client.getControl().getAngleNmb())) * power;
		        	double vy = Math.sin(Math.toRadians(client.getControl().getAngleNmb())) * power;
		        	double ta = 2 * vy / bullet.GRAVITY;
		        	double hd = vx * ta;
		        	double vd = vy * ta;
		        	Platform.runLater(() ->{
	        			game.getChildren().add(bullet);
					});
					final Timeline timeline = new Timeline();
					timeline.setCycleCount(1);
					timeline.setAutoReverse(false);
					bullet.setX(-10);
					KeyValue xKV = new KeyValue(bullet.xProperty(), hd - 10);
					KeyValue yKV = new KeyValue(bullet.yProperty(), -vd, new Interpolator() {
						@Override
						protected double curve(double t) {
							return -4 * (t - .5) * (t - .5) + 1;
						}
					});
					KeyFrame xKF = new KeyFrame(Duration.millis(2000), xKV);
					KeyFrame yKF = new KeyFrame(Duration.millis(2000), yKV);
					timeline.getKeyFrames().addAll(xKF, yKF);
					timeline.play();
					map.connection.setText("Ceka se da drugi igrac odigra!");
					client.control.setDisable(true);
					client.writeToServer("OK1," + (int)client.control.getSlider().getValue() + "," + client.control.getAngleNmb() + "," + bulet);
		        	timeline.setOnFinished(e -> {
		        		Platform.runLater(() ->{
		        			game.getChildren().remove(bullet);
		        		});
		        	String toRemove = client.getControl().getCb().getSelectionModel().getSelectedItem().toString();
		        	client.getControl().getCb().getItems().remove(toRemove);
		        	client.getControl().getCb().getSelectionModel().selectFirst();
		        	if(bullet.getBoundsInParent().intersects(player2Tank.getBoundsInParent())){
		        		if(toRemove.equals("Single Shot")) {
		        			map.setPlayer1Score(map.getPlayer1Score() + client.getControl().single.getDamage());
		        			map.getScore1().setText("Player 1:\n " + map.getPlayer1Score());
		        		}else if(toRemove.equals("Cutter")) {
		        			map.setPlayer1Score(map.getPlayer1Score() + client.getControl().cut.getDamage());
		        			map.getScore1().setText("Player 1:\n " + map.getPlayer1Score());
		        		}
		        	}else if(bullet.getBoundsInParent().intersects(player1Tank.getBoundsInParent())) {
		        		if(toRemove.equals("Single Shot")) {
		        			map.setPlayer1Score(map.getPlayer1Score() - client.getControl().single.getDamage());
		        			map.getScore1().setText("Player 1:\n " + map.getPlayer1Score());
		        		}else if(toRemove.equals("Cutter")) {
		        			map.setPlayer1Score(map.getPlayer1Score() - client.getControl().cut.getDamage());
		        			map.getScore1().setText("Player 1:\n " + map.getPlayer1Score());
		        		}
		        	}
		        	bulet = 0;
		        	});
				}
				if(client.player1.equals("1") && (test1.equals("OK2") || test1.equals("OK2END"))) {
					int bulet = client.bulet;
					if(client.bulet == 1) {
						bullet = new Bullet(player2Tank.getImg().getTranslateX(), player2Tank.getImg().getTranslateY(),player2Tank.getImg().getFitWidth(), bulet);
						
					}
					if(client.bulet == 2) {
						bullet = new Bullet(player2Tank.getImg().getTranslateX(), player2Tank.getImg().getTranslateY(),player2Tank.getImg().getFitWidth(), bulet);
					}
					double power = client.power;
					double vx = Math.cos(Math.toRadians(client.angle)) * power;
		        	double vy = Math.sin(Math.toRadians(client.angle)) * power;
		        	double ta = 2 * vy / bullet.GRAVITY;
		        	double hd = vx * ta;
		        	double vd = vy * ta;
		        	Platform.runLater(() -> {
	        			game.getChildren().add(bullet);
	        		});
					final Timeline timeline = new Timeline();
					timeline.setCycleCount(1);
					timeline.setAutoReverse(false);
					map.connection.setText("Vi ste na potezu!");
					bullet.setX(-10);
					KeyValue xKV = new KeyValue(bullet.xProperty(), hd - 10);
					KeyValue yKV = new KeyValue(bullet.yProperty(), -vd, new Interpolator() {
						@Override
						protected double curve(double t) {
							return -4 * (t - .5) * (t - .5) + 1;
						}
					});
					KeyFrame xKF = new KeyFrame(Duration.millis(2000), xKV);
					KeyFrame yKF = new KeyFrame(Duration.millis(2000), yKV);
					timeline.getKeyFrames().addAll(xKF, yKF);
					timeline.play();
		        	timeline.setOnFinished(e -> {
		        		Platform.runLater(() -> {
		        			game.getChildren().remove(bullet);
		        		});
		        	if(bullet.getBoundsInParent().intersects(player2Tank.getBoundsInParent())){
		        		if(bulet == 1) {
		        			map.setPlayer2Score(map.getPlayer2Score() - client.getControl().single.getDamage());
		        			map.getScore2().setText("Player 2:\n " + map.getPlayer2Score());
		        		}else if(bulet == 2) {
		        			map.setPlayer2Score(map.getPlayer2Score() - client.getControl().cut.getDamage());
		        			map.getScore2().setText("Player 2:\n " + map.getPlayer2Score());
		        		}
		        	}else if(bullet.getBoundsInParent().intersects(player1Tank.getBoundsInParent())) {
		        		if(bulet == 1) {
		        			map.setPlayer2Score(map.getPlayer2Score() + client.getControl().single.getDamage());
		        			map.getScore2().setText("Player 2:\n " + map.getPlayer2Score());
		        		}else if(bulet == 2) {
		        			map.setPlayer2Score(map.getPlayer2Score() + client.getControl().cut.getDamage());
		        			map.getScore2().setText("Player 2:\n " + map.getPlayer2Score());
		        		}
		        	}
		        	if(test1.equals("OK2END")) {
		        		test1 = "";
						client.stored2 = "";
			        	client.starts = "";
			        	client.bulet = 0;
			        	client.power = 0;
			        	client.angle = 0;
						client.writeToServer("OK3END" + "," + map.getPlayer1Score() + "," + map.getPlayer2Score());
						
					}
		        	test1 = "";
					client.stored2 = "";
		        	client.starts = "";
		        	client.bulet = 0;
		        	client.power = 0;
		        	client.angle = 0;
		        	});
				}
				if(client.player2.equals("2") && test1.equals("")) {
					if(client.control.getCb().getSelectionModel().getSelectedItem().toString().equals("Single Shot")) {
						bullet = new Bullet(player2Tank.getImg().getTranslateX(), player2Tank.getImg().getTranslateY(),player2Tank.getImg().getFitWidth(), 1);
						bulet = 1;
					}
					if(client.control.getCb().getSelectionModel().getSelectedItem().toString().equals("Cutter")) {
						bullet = new Bullet(player2Tank.getImg().getTranslateX(), player2Tank.getImg().getTranslateY(),player2Tank.getImg().getFitWidth(), 2);
						bulet = 2;
					}
					double power = client.getControl().getSlider().getValue();
					double vx = Math.cos(Math.toRadians(client.getControl().getAngleNmb())) * power;
		        	double vy = Math.sin(Math.toRadians(client.getControl().getAngleNmb())) * power;
		        	double ta = 2 * vy / bullet.GRAVITY;
		        	double hd = vx * ta;
		        	double vd = vy * ta;
		        	Platform.runLater(() -> {
		        			game.getChildren().add(bullet);
		        		});
					final Timeline timeline = new Timeline();
					timeline.setCycleCount(1);
					timeline.setAutoReverse(false);
					bullet.setX(-10);
					KeyValue xKV = new KeyValue(bullet.xProperty(), hd - 10);
					KeyValue yKV = new KeyValue(bullet.yProperty(), -vd, new Interpolator() {
						@Override
						protected double curve(double t) {
							return -4 * (t - .5) * (t - .5) + 1;
						}
					});
					KeyFrame xKF = new KeyFrame(Duration.millis(2000), xKV);
					KeyFrame yKF = new KeyFrame(Duration.millis(2000), yKV);
					timeline.getKeyFrames().addAll(xKF, yKF);
					timeline.play();
					map.connection.setText("Ceka se da prvi igrac odigra!");
					client.control.setDisable(true);
					if(client.getControl().getCb().getItems().size() == 1) {
						client.writeToServer("OK2END," + (int)client.control.getSlider().getValue() + "," + client.control.getAngleNmb() + "," + bulet);
					}else {
						client.writeToServer("OK2," + (int)client.control.getSlider().getValue() + "," + client.control.getAngleNmb() + "," + bulet);
					}
		        	timeline.setOnFinished(e -> {
		        		Platform.runLater(() -> {
		        			game.getChildren().remove(bullet);
		        		});
		        	String toRemove = client.getControl().getCb().getSelectionModel().getSelectedItem().toString();
		        	client.getControl().getCb().getItems().remove(toRemove);
		        	client.getControl().getCb().getSelectionModel().selectFirst();
		        	if(bullet.getBoundsInParent().intersects(player2Tank.getBoundsInParent())){
		        		if(toRemove.equals("Single Shot")) {
		        			map.setPlayer2Score(map.getPlayer2Score() - client.getControl().single.getDamage());
		        			map.getScore2().setText("Player 2:\n " + map.getPlayer2Score());
		        		}else if(toRemove.equals("Cutter")) {
		        			map.setPlayer2Score(map.getPlayer2Score() - client.getControl().cut.getDamage());
		        			map.getScore2().setText("Player 2:\n " + map.getPlayer2Score());
		        		}
		        	}else if(bullet.getBoundsInParent().intersects(player1Tank.getBoundsInParent())) {
		        		if(toRemove.equals("Single Shot")) {
		        			map.setPlayer2Score(map.getPlayer2Score() + client.getControl().single.getDamage());
		        			map.getScore2().setText("Player 2:\n " + map.getPlayer2Score());
		        		}else if(toRemove.equals("Cutter")) {
		        			map.setPlayer2Score(map.getPlayer2Score() + client.getControl().cut.getDamage());
		        			map.getScore2().setText("Player 2:\n " + map.getPlayer2Score());
		        		}
		        	}
		        	bulet = 0;
		        	});
				}
				if(client.player2.equals("2") && test1.equals("OK1")) {
					int bulet = client.bulet;
					if(client.bulet == 1) {
						bullet = new Bullet(player1Tank.getImg().getTranslateX(), player1Tank.getImg().getTranslateY(),player1Tank.getImg().getFitWidth(), bulet);
					}
					if(client.bulet == 2) {
						bullet = new Bullet(player1Tank.getImg().getTranslateX(), player1Tank.getImg().getTranslateY(),player1Tank.getImg().getFitWidth(), bulet);
					}
					double power = client.power;
					double vx = Math.cos(Math.toRadians(client.angle)) * power;
		        	double vy = Math.sin(Math.toRadians(client.angle)) * power;
		        	double ta = 2 * vy / bullet.GRAVITY;
		        	double hd = vx * ta;
		        	double vd = vy * ta;
		        	Platform.runLater(() -> {
	        			game.getChildren().add(bullet);
		        	});
					final Timeline timeline = new Timeline();
					timeline.setCycleCount(1);
					timeline.setAutoReverse(false);
					map.connection.setText("Vi ste na potezu!");
					bullet.setX(-10);
					KeyValue xKV = new KeyValue(bullet.xProperty(), hd - 10);
					KeyValue yKV = new KeyValue(bullet.yProperty(), -vd, new Interpolator() {
						@Override
						protected double curve(double t) {
							return -4 * (t - .5) * (t - .5) + 1;
						}
					});
					KeyFrame xKF = new KeyFrame(Duration.millis(2000), xKV);
					KeyFrame yKF = new KeyFrame(Duration.millis(2000), yKV);
					timeline.getKeyFrames().addAll(xKF, yKF);
					timeline.play();
					test1 = "";
					client.stored2 = "";
		        	client.starts = "";
		        	client.bulet = 0;
		        	client.power = 0;
		        	client.angle = 0;
		        	timeline.setOnFinished(e -> {
		        		Platform.runLater(() -> {
		        			game.getChildren().remove(bullet);
		        		});
		        	if(bullet.getBoundsInParent().intersects(player2Tank.getBoundsInParent())){
		        		if(bulet == 1) {
		        			map.setPlayer1Score(map.getPlayer1Score() + client.getControl().single.getDamage());
		        			map.getScore1().setText("Player 1:\n " + map.getPlayer1Score());
		        		}else if(bulet == 2) {
		        			map.setPlayer1Score(map.getPlayer1Score() + client.getControl().cut.getDamage());
		        			map.getScore1().setText("Player 1:\n " + map.getPlayer1Score());
		        		}
		        	}else if(bullet.getBoundsInParent().intersects(player1Tank.getBoundsInParent())) {
		        		if(bulet == 1) {
		        			map.setPlayer1Score(map.getPlayer1Score() - client.getControl().single.getDamage());
		        			map.getScore1().setText("Player 1:\n " + map.getPlayer1Score());
		        		}else if(bulet == 2) {
		        			map.setPlayer1Score(map.getPlayer1Score() - client.getControl().cut.getDamage());
		        			map.getScore1().setText("Player 1:\n " + map.getPlayer1Score());
		        		}
		        	}
		        	});
				}
			}
		});
			
		//dugme za podesavanje ugla cevke na gore
		client.getControl().getUpBtn().setOnAction(new EventHandler<javafx.event.ActionEvent>() {

			@Override
			public void handle(javafx.event.ActionEvent event) {
				
				test1 = client.stored2;
				if(client.player1.equals("1")) {
					if(client.getControl().getAngleNmb() < 180 && test1.equals("")) {
						rotation = new Rotate();
						rotation.setPivotX(player1Tank.getImg().getFitWidth() / 2);
						rotation.setPivotY(player1Tank.getImg().getFitHeight());
						rotation.setAngle(-1);
						client.getControl().setAngleNmb(1);
						client.getControl().angle.setText("Angle: \n" + client.getControl().getAngleNmb());
						player1Tank.getImg().getTransforms().add(rotation);
						client.writeToServer("UP1");
					}
					if(client.getControl().getAngleNmb() < 180 && test1.equals("UP2")) {
						rotation = new Rotate();
						rotation.setPivotX(player2Tank.getImg().getFitWidth() / 2);
						rotation.setPivotY(player2Tank.getImg().getFitHeight());
						rotation.setAngle(-1);
						player2Tank.getImg().getTransforms().add(rotation);
						test1 = "";
						client.stored2 = "";
						client.control.setDisable(true);
					}
				}
				if(client.player2.equals("2")) {
					if(client.getControl().getAngleNmb() < 180 && test1.equals("")) {
						rotation = new Rotate();
						rotation.setPivotX(player2Tank.getImg().getFitWidth() / 2);
						rotation.setPivotY(player2Tank.getImg().getFitHeight());
						rotation.setAngle(-1);
						client.getControl().setAngleNmb(1);
						client.getControl().angle.setText("Angle: \n" + client.getControl().getAngleNmb());
						player2Tank.getImg().getTransforms().add(rotation);
						Platform.runLater(() -> {
						client.writeToServer("UP2");
						});
					}
					if(client.getControl().getAngleNmb() < 180 && test1.equals("UP1")) {
						rotation = new Rotate();
						rotation.setPivotX(player1Tank.getImg().getFitWidth() / 2);
						rotation.setPivotY(player1Tank.getImg().getFitHeight());
						rotation.setAngle(-1);
						player1Tank.getImg().getTransforms().add(rotation);
						test1 = "";
						client.stored2 = "";
						client.control.setDisable(true);
					}
				}
			}
		});
		//dugme za podesavanje ugla cevke na dole
		client.getControl().getDownBtn().setOnAction(new EventHandler<javafx.event.ActionEvent>() {

			@Override
			public void handle(javafx.event.ActionEvent event) {
				test1 = client.stored2;
				if(client.player1.equals("1")) {
					if(client.getControl().getAngleNmb() > 0 && test1.equals("")) {
						rotation = new Rotate();
						rotation.setPivotX(player1Tank.getImg().getFitWidth() / 2);
						rotation.setPivotY(player1Tank.getImg().getFitHeight());
						rotation.setAngle(1);
						client.getControl().setAngleNmb(-1);
						client.getControl().angle.setText("Angle: \n" + client.getControl().getAngleNmb());
						player1Tank.getImg().getTransforms().add(rotation);
						client.writeToServer("DOWN1");
					}
					if(client.getControl().getAngleNmb() > 0 && test1.equals("DOWN2")) {
						rotation = new Rotate();
						rotation.setPivotX(player2Tank.getImg().getFitWidth() / 2);
						rotation.setPivotY(player2Tank.getImg().getFitHeight());
						rotation.setAngle(1);
						player2Tank.getImg().getTransforms().add(rotation);
						test1 = "";
						client.stored2 = "";
						client.control.setDisable(true);
					} 
				}
				if(client.player2.equals("2")) {
					if(client.getControl().getAngleNmb() > 0 && test1.equals("")) {
						rotation = new Rotate();
						rotation.setPivotX(player2Tank.getImg().getFitWidth() / 2);
						rotation.setPivotY(player2Tank.getImg().getFitHeight());
						rotation.setAngle(1);
						client.getControl().setAngleNmb(-1);
						client.getControl().angle.setText("Angle: \n" + client.getControl().getAngleNmb());
						player2Tank.getImg().getTransforms().add(rotation);
						client.writeToServer("DOWN2");
					}
					if(client.getControl().getAngleNmb() > 0 && test1.equals("DOWN1")) {
						rotation = new Rotate();
						rotation.setPivotX(player1Tank.getImg().getFitWidth() / 2);
						rotation.setPivotY(player1Tank.getImg().getFitHeight());
						rotation.setAngle(1);
						player1Tank.getImg().getTransforms().add(rotation);
						test1 = "";
						client.stored2 = "";
						client.control.setDisable(true);
					} 
				}
			}
		});
		//dugme za pomeranje tenka u levo
		client.getControl().getLeftBtn().setOnAction(new EventHandler<javafx.event.ActionEvent>() {

			@Override
			public void handle(javafx.event.ActionEvent event) {
				test1 = client.stored2;
				if(client.player1.equals("1")) {
					if(client.getControl().getMoves() > 0 && test1.equals("")) {
						trTank.setNode(player1Tank);
						trTank.setDuration(Duration.seconds(2));
						trTank.setByX(-40);
						trGun.setNode(player1Tank.getImg());
						trGun.setDuration(Duration.seconds(2));
						trGun.setByX(-40);
						client.getControl().getLeftBtn().setDisable(true);
						client.getControl().getRightBtn().setDisable(true);
						trGun.play();
						trTank.play();
						trTank.setOnFinished(ex -> {if(client.getControl().getLeftBtn().isDisable()) {
							client.getControl().getRightBtn().setDisable(false);
							client.getControl().getLeftBtn().setDisable(false);
						}});
						client.getControl().setlMoves();
						client.writeToServer("LEFT1");
					}
					if(client.getControl().getMoves() > 0 && test1.equals("LEFT2")) {
						trTank.setNode(player2Tank);
						trTank.setDuration(Duration.seconds(2));
						trTank.setByX(-40);
						trGun.setNode(player2Tank.getImg());
						trGun.setDuration(Duration.seconds(2));
						trGun.setByX(-40);
						trGun.play();
						trTank.play();
						test1 = "";
						client.stored2 = "";
						client.control.setDisable(true);
					}
				}
				if(client.player2.equals("2")) {
					if(client.getControl().getMoves() > 0 && test1.equals("")) {
						trTank.setNode(player2Tank);
						trTank.setDuration(Duration.seconds(2));
						trTank.setByX(-40);
						trGun.setNode(player2Tank.getImg());
						trGun.setDuration(Duration.seconds(2));
						trGun.setByX(-40);
						client.getControl().getLeftBtn().setDisable(true);
						client.getControl().getRightBtn().setDisable(true);
						trGun.play();
						trTank.play();
						trTank.setOnFinished(ex -> {if(client.getControl().getLeftBtn().isDisable()) {
							client.getControl().getRightBtn().setDisable(false);
							client.getControl().getLeftBtn().setDisable(false);
						}});
						client.getControl().setlMoves();
						client.writeToServer("LEFT2");
					}
					if(client.getControl().getMoves() > 0 && test1.equals("LEFT1")) {
						trTank.setNode(player1Tank);
						trTank.setDuration(Duration.seconds(2));
						trTank.setByX(-40);
						trGun.setNode(player1Tank.getImg());
						trGun.setDuration(Duration.seconds(2));
						trGun.setByX(-40);
						trGun.play();
						trTank.play();
						test1 = "";
						client.stored2 = "";
						client.control.setDisable(true);
					}
				}
		}});
			
		//dugme za pomeranje tenka u desno
		client.getControl().getRightBtn().setOnAction(new EventHandler<javafx.event.ActionEvent>() {

			@Override
			public void handle(javafx.event.ActionEvent event) {
				test1 = client.stored2;
				if(client.player1.equals("1")) {
					if(client.getControl().getMoves() > 0 && test1.equals("")) {
						trTank.setNode(player1Tank);
						trTank.setDuration(Duration.seconds(2));
						trTank.setByX(40);
						trGun.setNode(player1Tank.getImg());
						trGun.setDuration(Duration.seconds(2));
						trGun.setByX(40);
						client.getControl().getRightBtn().setDisable(true);
						client.getControl().getLeftBtn().setDisable(true);
						trGun.play();
						trTank.play();
						trTank.setOnFinished(ex -> {if(client.getControl().getRightBtn().isDisable()) {
							client.getControl().getRightBtn().setDisable(false);
							client.getControl().getLeftBtn().setDisable(false);
						}});
						client.getControl().setlMoves();
						client.writeToServer("RIGHT1");
					}
					if(client.getControl().getMoves() > 0 && test1.equals("RIGHT2")) {
						trTank.setNode(player2Tank);
						trTank.setDuration(Duration.seconds(2));
						trTank.setByX(40);
						trGun.setNode(player2Tank.getImg());
						trGun.setDuration(Duration.seconds(2));
						trGun.setByX(40);
						trGun.play();
						trTank.play();
						test1 = "";
						client.stored2 = "";
						client.control.setDisable(true);
					}
				}
				if(client.player2.equals("2")) {
					if(client.getControl().getMoves() > 0 && test1.equals("")) {
						trTank.setNode(player2Tank);
						trTank.setDuration(Duration.seconds(2));
						trTank.setByX(40);
						trGun.setNode(player2Tank.getImg());
						trGun.setDuration(Duration.seconds(2));
						trGun.setByX(40);
						client.getControl().getRightBtn().setDisable(true);
						client.getControl().getLeftBtn().setDisable(true);
						trGun.play();
						trTank.play();
						trTank.setOnFinished(ex -> {if(client.getControl().getRightBtn().isDisable()) {
							client.getControl().getRightBtn().setDisable(false);
							client.getControl().getLeftBtn().setDisable(false);
						}});
						client.getControl().setlMoves();
						client.writeToServer("RIGHT2");
					}
					if(client.getControl().getMoves() > 0 && test1.equals("RIGHT1")) {
						trTank.setNode(player1Tank);
						trTank.setDuration(Duration.seconds(2));
						trTank.setByX(40);
						trGun.setNode(player1Tank.getImg());
						trGun.setDuration(Duration.seconds(2));
						trGun.setByX(40);
						trGun.play();
						trTank.play();
						test1 = "";
						client.stored2 = "";
						client.control.setDisable(true);
					}
				}
		}});
			
		//menjanje vrednosti na slajderu
		client.getControl().getSlider().valueProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> ov, Number oldValue, Number newValue) {
				int val = newValue.intValue();
				client.getControl().getSliderValue().setText("Power: " + val);
			}
		});
		return game;
	}
	
	@Override
	public void start(Stage primaryStage) {
			stage = primaryStage;
			scene1 = new Scene(createContentScene1());
			primaryStage.setScene(scene1);
			primaryStage.setTitle("Pocket tanks");
			primaryStage.setResizable(false);
			primaryStage.show();
	}
	//odavde pocinje startovanje klijenta
	public static void main(String[] args) {
		launch();
	}
	
	
}
