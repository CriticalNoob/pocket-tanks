package objects;

import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class Bullet extends ImageView{

	private Image bulletImage2 = new Image("file:images/weapons/cutter/cutter1.png");
	private Image bulletImage1 = new Image("file:images/weapons/single_shot/single_shot_1.png");
	public final double GRAVITY = 9.8;
	
	public Bullet(double gunX, double gunY, double gunWidth, int nummber) {
		if(nummber == 1){
			this.setImage(bulletImage1);
		}
		if(nummber == 2) {
			this.setImage(bulletImage2);
		}
		this.setTranslateX(gunX + gunWidth / 2);
		this.setTranslateY(gunY);
		this.setVisible(true);
	}

	public Image getBulletImage1() {
		return bulletImage1;
	}
	public Image getBulletImage2() {
		return bulletImage2;
	}
}
