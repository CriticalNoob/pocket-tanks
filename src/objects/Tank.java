package objects;

import enviroment.GameMap;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.transform.Rotate;

public class Tank extends ImageView{

	private PlayerTanks players;
	private GameMap map = new GameMap();
	private Image tank1Image = new Image("file:images/tank1/tank1.png");
	private Image tank2Image = new Image("file:images/tank2/tank1.png");
	private Image tankGun = new Image("file:images/tankGun/TankGun.png");
	private double tankImageHeight = 32;
	private double tankImageWidth = 64;
	private ImageView img = new ImageView();
	
	public Tank(PlayerTanks tank) {
		
		this.setFitHeight(tankImageHeight);
		this.setFitWidth(tankImageWidth);
		
		if(tank.equals(players.Player1)){
			this.setImage(this.getTank1Image());
			this.setTranslateX(map.getRecCenter().getWidth()/10);
			this.setTranslateY((map.getRecCenter().getHeight() + map.getRecKonekcija().getHeight()) - this.getFitHeight());
			this.setGun(tank);
		}else {
			this.setImage(this.getTank2Image());
			this.setTranslateX(map.getRecCenter().getWidth() - (map.getRecCenter().getWidth()/10)- this.getFitWidth());
			this.setTranslateY((map.getRecCenter().getHeight() + map.getRecKonekcija().getHeight()) - this.getFitHeight());
			this.setGun(tank);
		}
	}
	
	public void setGun(PlayerTanks p1) {
		img.setImage(tankGun);
		img.setFitHeight(15);
		img.setFitWidth(3);
		if(p1.equals(players.Player1)){
			img.setTranslateX((map.getRecCenter().getWidth()/10 + tankImageWidth/2));
			img.setTranslateY((map.getRecCenter().getHeight() + map.getRecKonekcija().getHeight()) - this.getFitHeight() + tankImageHeight/2 - img.getFitHeight());
		}else {
			img.setTranslateX(map.getRecCenter().getWidth() - (map.getRecCenter().getWidth()/10)- this.getFitWidth() + tankImageWidth/2);
			img.setTranslateY((map.getRecCenter().getHeight() + map.getRecKonekcija().getHeight()) - this.getFitHeight() + tankImageHeight/2 - img.getFitHeight());
		}
	}
	
	public ImageView getImg() {
		return img;
	}

	public Image getTank1Image() {
		return tank1Image;
	}
	
	public Image getTank2Image() {
		return tank2Image;
	}
	
	
}
